"""Console script for tool_app."""

import click
from . import tool_app


@click.command()
@click.option('--count', default=1)
def main(count):
    """Console script for tool_app."""
    tool_app.hello()


if __name__ == "__main__":
    main()
