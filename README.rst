========
tool-app
========


.. image:: https://img.shields.io/pypi/v/tool_app.svg
        :target: https://pypi.python.org/pypi/tool_app

.. image:: https://img.shields.io/travis/raranjan/tool_app.svg
        :target: https://travis-ci.org/raranjan/tool_app

.. image:: https://readthedocs.org/projects/tool-app/badge/?version=latest
        :target: https://tool-app.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/raranjan/tool_app/shield.svg
     :target: https://pyup.io/repos/github/raranjan/tool_app/
     :alt: Updates


Application to track the productivity of team


* Free software: MIT license
* Documentation: https://tool-app.readthedocs.io.


Features
--------

* TODO

Credits
-------------
Rakesh Ranjan
